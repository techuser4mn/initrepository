package org.testsample.controllers;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.testsample.model.entity.Item;
import org.testsample.services.LanguageService;


@RestController
public class LangController
{
    @Autowired
    private LanguageService service;

    private static Logger log = LogManager.getLogger(LangController.class);
    
    @GetMapping("/language")
    public List<Item> getAllItems()
    {
        List<Item> result = service.findAll();
        log.error("Get all items, result: [{}];", result);
        
        return result;
    }
    
    @GetMapping("/language/{name}")
    public ResponseEntity<Item> getItemByName(@PathVariable String name)
    {
        Item result = service.findByName(name);
        log.debug("Get item by name, name:[{}], result: [{}];",name, result);
        
        ResponseEntity<Item> entity  = new ResponseEntity<>(result, HttpStatus.OK);
        if (Objects.isNull((result)))
                entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
      
        return entity;
    }
    
    @PostMapping("/language")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> addItem(@Valid @RequestBody Item item)
    {
        log.debug("Add new item, value :[{}];", item);

        Item result = service.save(item);
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("state", HttpStatus.OK);
        response.put("body", result);

        return response; 
    }

    @PutMapping("language/{name}")
    public ResponseEntity<?> updateItem(@RequestBody Item newItem, @PathVariable String name)
    {
        Item item = service.findByName(name);

        log.debug("Update item, name: [{}], value: [{}];", name, newItem);

        Map<String, Object> response = new LinkedHashMap<>();
        ResponseEntity<?> entity  = new ResponseEntity<>(HttpStatus.OK);
        if (Objects.nonNull(item))
        {
            item.setDescription(newItem.getDescription());
            item.setRating(newItem.getRating());
            Item result = service.save(item);

            response.put("state", HttpStatus.OK);
            response.put("body", result);
            entity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
        }
        else
        {
            entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        return entity;
    }
    
    @DeleteMapping("/language/{name}")
    public ResponseEntity<?> deleteItem(@PathVariable String name)
    {
        log.info("Delete item, name: [{}];", name);

        Item item = service.findByName(name);

        Map<String, Object> response = new LinkedHashMap<>();
        ResponseEntity<?> entity  = new ResponseEntity<>(HttpStatus.OK);
        if (Objects.nonNull(item))
        {
            service.delete(item);
            response.put("state", HttpStatus.OK);
            entity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
            
        }
        else
        {
            entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        return entity;
   }
}
