package org.testsample.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name = "languages",
    uniqueConstraints =  { 
            @UniqueConstraint(columnNames = {"id"}),
            @UniqueConstraint(columnNames = {"name"})
    }
)

public class Item
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;
    
    @Pattern(regexp = "Java|JavaScript|C\\#|C\\+\\+|Python", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String name;
    @NotNull
    private String description;
    
    @Min(1)
    @Max(5)
    private int rating;
}
