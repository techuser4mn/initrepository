package org.testsample.services;
import org.springframework.data.jpa.repository.JpaRepository;
import org.testsample.model.entity.Item;

public interface LanguageService extends JpaRepository<Item, Long>
{
    Item findByName(String name);
}
