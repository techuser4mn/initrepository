USE `sampleapp`

CREATE TABLE `languages`
(
    `id`            BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name`          VARCHAR(100)    NOT NULL,
    `description`   VARCHAR(1000)
    `rating`        INT UNSIGNED NOT NULL,
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = 'Language dictionary';
