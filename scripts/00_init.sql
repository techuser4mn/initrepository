CREATE DATABASE `sampleapp` DEFAULT CHARACTER SET utf8;

GRANT ALL ON `sampleapp`.* TO 'sampleuser'@'%' IDENTIFIED BY 'password';
GRANT ALL ON `sampleapp`.* TO 'sampleuser'@'localhost' IDENTIFIED BY 'password';
